/**
 * Plugin Name: 论坛展示型app
 * Description: uloug.com To develop and share! Please keep the copyright information!
 * Author: 拾讯网
 * Author URI: http://www.uloug.com
 * Version: 1.0
 * Plugin URI: https://git.oschina.net/858598758/apicloud.mao10.app.demo.git
 * License: GPL2+
 */

//数据对接
//@tic 文章页
var tic_url_id = 'http://www.mao10.com';
var tic_url_list = '/do/json/postlist.php';
var tic_url_con = '/do/json/post.php';

//@goods 商品
var goods_url_id = 'http://www.xinpinghui.com';
var goods_url_list = '/index.php/api/';
var goods_url_com = '/index.php/api/getandorid';

//网络判断
//判断网络类型
function appnetwork() {
	var connectionType = api.connectionType;
	if (connectionType == 'none') {
		network_no();
	} else {
		network_yes();
	};
}

//没网时显示
function network_no() {
	var net_html = '';

	net_html += '<div id="network_no"><style>#network_no { text-align: center; padding: 20% 15px 0 15px; width: 100%; } #network_no img { width: 85px; } '
	net_html += '#network_no span { font-weight: 600; color: #420B0B; } #network_no .aui-btn-outlined { width: 46%; padding: 7px 0; display: inline-block; border-color: #420B0B; color: #420B0B; } '
	net_html += '#network_no .aui-btn-outlined:active { background-color: #f00; color: #ffffff; }</style>'
	net_html += '<div class="aui-content"><img src="widget://image/aui-icon.png" alt="" /><p></p><span>糟糕！加载失败！</span>'
	net_html += '</div><div class="aui-content"><p>目前可公开的原因：</p><p>1.和网络君的对接请求失败</p><p>2.内容被库洛里大魔法师召唤走了</p>'
	net_html += '</div><div class="aui-content"><p>目前不可公开的原因：</p><p>3.拾讯君心情不好，不想给你看( ¯ε¯ )</p>'
	net_html += '</div><div class="aui-btn aui-btn-block aui-btn-outlined" tapmode onclick="appnetwork()">'
	net_html += '戳我重试</div></div>'
	
	$api.html($api.dom('body'), net_html);
	
}
//没网时刷新
//function netRefresh() {
//	appnetwork();
//}

//关闭
function closeWin() {
	api.closeWin();
}

// 显示加载进度
function showProgress() {
	api.showProgress({
		style : 'default',
		animationType : 'fade',
		title : '努力加载中...',
		text : '先喝杯茶...',
		modal : false
	});
}

// 打开通用Win
function openWin(name) {
	var delay = 0;
	if (api.systemType != 'ios') {
		delay = 300;
	}
	api.openWin({
		name : '' + name + '_win',
		url : '' + name + '_win.html',
		bounces : false,
		delay : delay,
		slidBackEnabled : true,
		vScrollBarEnabled : false
	});
}

// 打开信息Win
function news() {
	var delay = 0;
	if (api.systemType != 'ios') {
		delay = 300;
	}
	api.openWin({
		name : 'news_win',
		url : '../../html/news/news_win.html',
		bounces : false,
		delay : delay,
		slidBackEnabled : true,
		vScrollBarEnabled : false
	});
}

// echo延迟加载
function echoInit() {
	echo.init({
		offset : 0,
		throttle : 0 //设置图片延迟加载的时间
	});
}
//下拉刷新声明
var set_Header_text = {
bgColor: '#f0f0f0',
image: {
pull: [
'widget://image/dropdown_anim_00.png',
'widget://image/dropdown_anim_01.png',
'widget://image/dropdown_anim_02.png',
'widget://image/dropdown_anim_03.png',
'widget://image/dropdown_anim_04.png',
'widget://image/dropdown_anim_05.png',
'widget://image/dropdown_anim_06.png',
'widget://image/dropdown_anim_07.png',
'widget://image/dropdown_anim_08.png',
'widget://image/dropdown_anim_09.png',
'widget://image/dropdown_anim_10.png',
],
load: [
'widget://image/dropdown_loading_00.png',
'widget://image/dropdown_loading_01.png',
'widget://image/dropdown_loading_02.png'
]}
};

